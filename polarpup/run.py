from urllib.parse import urlparse
from pathlib import Path
import tempfile

import pandas as pd
import xarray
import rasterio
from rasterio.warp import reproject, Resampling
import globus_sdk
from google.cloud import storage
import numpy as np
from scipy import ndimage

from polarpup.core.corrstats import compute_corr_stats
from polarpup.core.roughness import compute_roughness, compute_roughness_lowmem
import polarpup.core.granules as granules
from polarpup.core.mosaic import gdal_merge
import polarpup.core.google as google
import polarpup.core.globus as globus
import polarpup.core.grid as grid


def generate_corr_stats(filepath, search_dir, output_dir, granule_index_filepath=None):
    if granule_index_filepath is not None:
        granule_index = pd.read_parquet(granule_index_filepath)
    else:
        granule_index = None

    mask = compute_corr_stats(filepath=filepath, search_dir=search_dir, granule_index=granule_index)
    mask.attrs['source_filepath'] = filepath

    mask.to_netcdf(Path(output_dir) / granules.corr_stats_filename(filepath))


def generate_roughness(filepath, output_dir):
    with rasterio.open(filepath, 'r') as src:
        source_affine = src.affine
        data = src.read(1).astype(float)

    nodata = 0
    mask = (data == nodata)
    roughness, mask = compute_roughness(data, mask)
    roughness, final_affine = grid.resample(roughness,
                                            source_affine,
                                            factor=1/20.)
    mask, final_affine = grid.resample(mask,
                                       source_affine,
                                       factor=1/20.)

    ds = grid.init_polarstereo_dataset(shape=roughness.shape, affine=final_affine)
    ds.attrs['source_filepath'] = filepath

    roughness[mask] = np.nan
    grid.add_polarstereo_grid(dataset=ds, name='roughness', data=roughness)

    output_filepath = Path(output_dir) / granules.roughness_filename(filepath)
    ds.to_netcdf(output_filepath)


def generate_roughness_lowmem(filepath, output_dir):
    nodata = 0
    with rasterio.open(filepath, 'r') as input_raster:
        source_affine = input_raster.affine
        source_profile = input_raster.profile
        temp_profile = source_profile
        temp_profile['dtype'] = 'float32'
        temp_profile['nodata'] = nodata
        with tempfile.NamedTemporaryFile('w') as temp_file:
            with rasterio.open(temp_file.name, 'w', **source_profile) as temp_raster:
                compute_roughness_lowmem(input_raster, temp_raster, nodata=nodata)
                roughness = temp_raster.read(1)
                mask = (roughness == nodata)
                roughness[mask] = np.nan
                roughness, final_affine = grid.resample(roughness,
                                                        source_affine,
                                                        factor=1/20.)

                ds = grid.init_polarstereo_dataset(shape=roughness.shape, affine=final_affine)
                ds.attrs['source_filepath'] = filepath

                grid.add_polarstereo_grid(dataset=ds, name='roughness', data=roughness)

                output_filepath = Path(output_dir) / granules.roughness_filename(filepath)
                ds.to_netcdf(output_filepath)


def generate_roughness_batch_script(filepath, output_dir, output_filepath):
    granules = pd.read_parquet(filepath)
    with open(output_filepath, 'w') as output_file:
        for filepath in granules.filepath:
            output_file.write(f'polarpup roughness -f {filepath} -o {output_dir}\n')


def generate_mask(filepath, corr_stats_filepath, output_dir):
    corr_stats_ds = xarray.open_dataset(corr_stats_filepath)
    corr_stats_affine = grid.get_affine(corr_stats_ds)
    corr_stats_shape = grid.get_shape(corr_stats_ds)

    masks = {
        'corr_max_thresh': (corr_stats_ds.corr_max < .3),
        'corr_ratio': (corr_stats_ds.corr_ratio < 1),
        'corr_max_ratio': (corr_stats_ds.corr_max_ratio < .8)
    }

    ds = grid.init_polarstereo_dataset(shape=corr_stats_shape,
                                       affine=corr_stats_affine)
    for name, mask in masks.items():
        mask = ndimage.morphology.binary_closing(np.array(mask), iterations=1)
        mask_ds = grid.add_polarstereo_grid(ds, name, mask.astype(int))
        mask_ds.attrs['_FillValue'] = 0

    ds.attrs['source_filepath'] = filepath

    output_filepath = Path(output_dir) / granules.mask_filename(filepath)
    ds.to_netcdf(output_filepath)


def apply_mask(filepath, mask_filepath, output_dir,
               mask_var='corr_max_thresh', target_var='roughness'):
    mask_ds = xarray.open_dataset(mask_filepath)
    mask_affine = grid.get_affine(mask_ds)
    target_ds = xarray.open_dataset(filepath)
    target_affine = grid.get_affine(target_ds)
    target_shape = grid.get_shape(target_ds)

    mask = np.array(mask_ds[mask_var])
    mask[np.isnan(mask)] = 0
    aligned_mask = np.empty(shape=target_shape)
    reproject(
        mask.astype(float), aligned_mask,
        src_transform=mask_affine,
        dst_transform=target_affine,
        src_crs=grid.POLARSTEREO_CRS,
        dst_crs=grid.POLARSTEREO_CRS,
        resampling=Resampling.nearest)

    masked = np.array(target_ds[target_var])
    masked[aligned_mask.astype(bool)] = np.nan

    ds = grid.init_polarstereo_dataset(shape=target_shape, affine=target_affine)
    grid.add_polarstereo_grid(ds, target_var, masked)
    output_filepath = Path(output_dir) / granules.masked_filename(filepath)
    ds.to_netcdf(output_filepath)


def find_granules(dataproduct, output_filepath, start_date, end_date, pieces, search_dir):
    results = granules.find_granules(dataproduct=dataproduct,
                                     search_dir=search_dir,
                                     start_date=start_date,
                                     end_date=end_date,
                                     pieces=pieces)

    results.filepath = results.filepath.apply(str)

    results.to_parquet(output_filepath)


def filter_granules(input_filepath, output_filepath,
                    start_date=None, end_date=None,
                    shapefile=None):
    results = pd.read_parquet(input_filepath)
    if shapefile:
        results = granules.filter_intersecting_shapefile(results, shapefile)
    if start_date and end_date:
        results = results[start_date <= results.date <= end_date]

    results.to_parquet(output_filepath)


def index_google(output_filepath):
    results = google.get_index()

    results.to_parquet(output_filepath)


def download_from_google(filepath, output_dir):
    client = storage.Client()
    bucket = client.bucket('gcp-public-data-landsat')
    url_parts = urlparse(filepath)
    blob_name = url_parts.path
    blob = bucket.get_blob(blob_name)

    dest_filepath = Path(output_dir) / filepath.name
    blob.download_to_filename(dest_filepath)


def index_globus(directory, output_filepath):
    results = globus.get_index(directory)

    results.to_parquet(output_filepath)


def cancel_globus():
    transfer_client = globus_sdk.TransferClient(authorizer=globus.get_authorizer())
    tasks = transfer_client.task_list(None)
    for task in tasks:
        transfer_client.cancel_task(task['task_id'])


def last_in_mosaic(filelist, output_filepath):
    gdal_merge(filelist, output_filepath)
