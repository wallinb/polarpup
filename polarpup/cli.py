import datetime as dt

import click

import polarpup.run as run
from polarpup.constants import DATAPRODUCTS


class DateType(click.ParamType):
    name = 'date'

    def convert(self, value, param, ctx):
        try:
            return dt.datetime.strptime(value, '%Y-%m-%d').date()
        except ValueError:
            self.fail('%s is not a valid %%Y-%%m-%%d date' % value, param, ctx)


dataproduct_choices = click.Choice(DATAPRODUCTS)


@click.group()
def polarpup():
    pass


@polarpup.command()
def cancel_globus(**kwargs):
    run.cancel_globus(**kwargs)


@polarpup.command()
@click.option('-d', '--directory',
              help='Globus directory to index')
@click.option('-o', '--output', 'output_filepath',
              default='globus-index.parquet',
              help='Output filepath for index')
def index_globus(**kwargs):
    run.index_globus(**kwargs)


@polarpup.command()
@click.option('-o', '--output', 'output_filepath',
              default='google-index.parquet',
              help='Output filepath for index')
def index_google(**kwargs):
    run.index_google(**kwargs)


@polarpup.command()
@click.option('-d', '--directory', 'search_dir',
              required=True,
              help='Directory to search')
@click.option('--type', 'dataproduct',
              type=dataproduct_choices,
              default=DATAPRODUCTS.LANDSAT8,
              help='Granule type')
@click.option('-o', '--output', 'output_filepath',
              default='granules.parquet',
              help='Output filepath for results')
@click.option('-s', '--start-date', type=DateType(),
              help='Start date (YYYY-MM-DD)')
@click.option('-e', '--end-date', type=DateType(),
              help='End date (YYYY-MM-DD)')
@click.option('--pieces',
              help='Dictionary of filename parts to filter on')
def find_granules(**kwargs):
    run.find_granules(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input granules index filepath')
@click.option('-o', '--output', 'output_filepath',
              default='granules.parquet',
              help='Output filepath for index')
@click.option('--type',
              type=dataproduct_choices,
              default=DATAPRODUCTS.LANDSAT8,
              help='Granule type')
def augment_metadata(**kwargs):
    run.add_metadata(**kwargs)


@polarpup.command()
@click.option('-i', '--input', 'input_filepath', required=True)
@click.option('-o', '--output', 'output_filepath',
              default='granules.parquet',
              help='Output filepath for results')
@click.option('-r', '--shapefile', 'shapefile',
              default='filtered_granules.parquet',
              help='Output filepath for index')
@click.option('-s', '--start-date', type=DateType(),
              help='Start date (YYYY-MM-DD)')
@click.option('-e', '--end-date', type=DateType(),
              help='End date (YYYY-MM-DD)')
def filter_granules(**kwargs):
    run.filter_granules(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input image filepath')
@click.option('-o', '--out-dir', 'output_dir',
              default='.',
              help='Output directory')
def roughness(**kwargs):
    run.generate_roughness_lowmem(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input granule list')
@click.option('-s', '--output', 'output_filepath',
              default='.',
              help='Output filepath for batch script')
@click.option('-o', '--out-dir', 'output_dir',
              default='.',
              help='Output directory')
def roughness_batch(**kwargs):
    run.generate_roughness_batch_script(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input landsat8 image filepath')
@click.option('-s', '--search-dir',
              help='Directory to search for NSIDC0710 granules')
@click.option('-g', '--granule-index', 'granule_index_filepath',
              help='Index of NSIDC0710 granules')
@click.option('-o', '--out-dir', 'output_dir',
              default='.',
              help='Output directory')
def corr_stats(**kwargs):
    run.generate_corr_stats(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input landsat8 image filepath')
@click.option('-c', '--corr-stats-filepath',
              help='Associated corr-stats file')
@click.option('-o', '--out-dir', 'output_dir',
              default='.',
              help='Output directory')
def mask(**kwargs):
    run.generate_mask(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input landsat8 image filepath')
@click.option('-m', '--mask-filepath',
              help='Associated corr-stats file')
@click.option('-o', '--out-dir', 'output_dir',
              default='.',
              help='Output directory')
def mask_roughness(**kwargs):
    run.apply_mask(**kwargs)


@polarpup.command()
@click.option('-f', '--filepath',
              required=True,
              help='Input landsat8 image filepath')
@click.option('-o', '--out-dir', 'output_dir',
              default='.',
              help='Output directory')
def download(**kwargs):
    run.download_from_google(**kwargs)


if __name__ == '__main__':
    polarpup()
