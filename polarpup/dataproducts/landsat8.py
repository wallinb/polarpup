from pathlib import Path
from datetime import datetime
from itertools import chain

from toolz import get_in, merge, curry
import pandas as pd

from polarpup.dataproducts.api import (
    glob_files, filename_pattern, pattern_config, match_filename)
from polarpup.config import CONFIG
from polarpup.constants import DATAPRODUCTS

DATAPRODUCT = DATAPRODUCTS.LANDSAT8
FILENAME_PATTERN = filename_pattern(DATAPRODUCT)
METADATA_KEYS = list(pattern_config(DATAPRODUCT)['pieces']) + [
    'collected_date'
]


def get_metadata_item(filepath, key):
    filepath = Path(filepath)
    if key == 'collected_date':
        pieces = match_filename(filename_pattern=FILENAME_PATTERN,
                                filename=filepath.name)
        date = datetime.strptime(
            '{collected_year}{collected_month}{collected_day}'.format(**pieces),
            '%Y%m%d').date()

        return date
    elif key in list(CONFIG[DATAPRODUCT]['filename_pattern']['pieces']):
        pieces = match_filename(filename_pattern=FILENAME_PATTERN,
                                filename=filepath.name)
        return pieces[key]
    else:
        raise RuntimeError(f'{DATAPRODUCT}: Unknown metadata key for "{key}"')


def find_granules(start_date=None, end_date=None, pieces=None, search_dirs=None):
    if search_dirs is None:
        search_dirs = get_in([DATAPRODUCT, 'search_dirs'], CONFIG, no_default=True)
    common_pieces = {} if pieces is None else pieces

    get_globs = curry(glob_files)(filename_pattern=FILENAME_PATTERN, search_dirs=search_dirs)

    if start_date is None and end_date is None:
        globs = [get_globs(pieces=pieces)]
    elif None in (start_date, end_date):
        date = start_date if start_date is not None else end_date
        globs = [get_globs(pieces=merge(common_pieces, _date_pieces(date)))]
    else:
        globs = []
        for date in pd.date_range(start_date, end_date):
            globs.append(get_globs(pieces=merge(common_pieces, _date_pieces(date))))

    globs = chain(*globs)

    return globs


def _date_pieces(date):
    pieces = {
        'collected_year': date.strftime('%Y'),
        'collected_month': date.strftime('%m'),
        'collected_day': date.strftime('%d')
    }

    return pieces
