from pathlib import Path
from datetime import datetime
from itertools import chain

from toolz import get_in, merge, curry
import pandas as pd

from polarpup.dataproducts.api import (
    glob_files, filename_pattern, pattern_config, match_filename)
from polarpup.config import CONFIG
from polarpup.constants import DATAPRODUCTS

DATAPRODUCT = DATAPRODUCTS.NSIDC0710
FILENAME_PATTERN = filename_pattern(DATAPRODUCT)
METADATA_KEYS = list(pattern_config(DATAPRODUCT)['pieces']) + [
    'date_a',
    'date_b'
]


def get_metadata_item(filepath, key):
    filepath = Path(filepath)
    if key == 'date_a':
        pieces = match_filename(filename_pattern=FILENAME_PATTERN,
                                filename=filepath.name)
        date_a = datetime.strptime('{year_a}-{doy_a}'.format(**pieces), '%Y-%j').date()

        return date_a
    if key == 'date_b':
        pieces = match_filename(filename_pattern=FILENAME_PATTERN,
                                filename=filepath.name)
        date_b = datetime.strptime('{year_b}-{doy_b}'.format(**pieces), '%Y-%j').date()

        return date_b
    elif key in list(CONFIG[DATAPRODUCT]['filename_pattern']['pieces']):
        pieces = match_filename(filename_pattern=FILENAME_PATTERN,
                                filename=filepath.name)
        return pieces[key]
    else:
        raise RuntimeError(f'{DATAPRODUCT}: Unknown metadata key for "{key}"')


def find_granules(start_date=None, end_date=None, pieces=None, search_dirs=None):
    if search_dirs is None:
        search_dirs = get_in([DATAPRODUCT, 'search_dirs'], CONFIG, no_default=True)
    common_pieces = {} if pieces is None else pieces

    get_globs = curry(glob_files)(filename_pattern=FILENAME_PATTERN, search_dirs=search_dirs)

    if start_date is None and end_date is None:
        globs = [get_globs(pieces=common_pieces)]
    elif None in (start_date, end_date):
        globs = []
        date = start_date if start_date is not None else end_date
        globs.extend([get_globs(pieces=merge(common_pieces, _date_pieces_a(date))),
                      get_globs(pieces=merge(common_pieces, _date_pieces_b(date)))])
    else:
        globs = []
        for date in pd.date_range(start_date, end_date):
            globs.extend([get_globs(pieces=merge(common_pieces, _date_pieces_a(date))),
                          get_globs(pieces=merge(common_pieces, _date_pieces_b(date)))])

    return chain(*globs)


def _date_pieces_a(date):
    pieces = {
        'year_a': date.strftime('%Y'),
        'doy_a': date.strftime('%j')
    }

    return pieces


def _date_pieces_b(date):
    pieces = {
        'year_b': date.strftime('%Y'),
        'doy_b': date.strftime('%j')
    }

    return pieces
