import pathlib  # Note: must be here for pytest/pyfakefs auto-patching to work
from collections import defaultdict
import re
from itertools import chain
from importlib import import_module

from toolz import get_in

from polarpup.config import CONFIG
from polarpup.dataproducts.util import FilenamePattern, FilenamePatternError


def pattern_config(dataproduct):
    return get_in([dataproduct, 'filename_pattern'], CONFIG, no_default=True)


def filename_pattern(dataproduct):
    pattern = FilenamePattern(**pattern_config(dataproduct))

    return pattern


def match_filename(filename_pattern, filename):
    pieces = filename_pattern.match(filename)
    if pieces is None:
        raise FilenamePatternError(Exception)

    return pieces


def glob_files(filename_pattern, search_dirs, pieces):
    kwargs = defaultdict(lambda: '*')
    if pieces is not None:
        kwargs.update(pieces)
    filename_glob = str(filename_pattern.format_map(kwargs))
    filename_glob = re.sub('\*\*+', '*', filename_glob)

    filepaths = chain(*[pathlib.Path(search_dir).glob('**/' + filename_glob)
                        for search_dir in search_dirs])

    return filepaths


def _dataproduct_attr(dataproduct, name):
    f = getattr(import_module(f'polarpup.dataproducts.{dataproduct.lower()}'), name)

    return f


def find_granules(dataproduct, start_date=None, end_date=None, pieces=None, search_dirs=None):
    return _dataproduct_attr(dataproduct, 'find_granules')(
        start_date=start_date,
        end_date=end_date,
        pieces=pieces,
        search_dirs=search_dirs)


def get_metadata(dataproduct, filepath):
    metadata = {key: get_metadata_item(dataproduct=dataproduct, key=key, filepath=filepath)
                for key in _dataproduct_attr(dataproduct, 'METADATA_KEYS')}

    return metadata


def get_metadata_item(dataproduct, filepath, key):
    return _dataproduct_attr(dataproduct, 'get_metadata_item')(filepath=filepath, key=key)


def filename_metadata_keys(dataproduct):
    return list(CONFIG[dataproduct]['filename_pattern']['pieces'])
