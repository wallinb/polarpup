import os
from pathlib import Path

import yaml


CONFIG = {}
try:
    CONFIG_PATH = Path(os.environ['POLARPUP_CONFIG_DIR'])
except KeyError:
    CONFIG_PATH = Path(__file__).parent / 'config'


def load_config(config_path=None):
    if config_path is None:
        config_path = CONFIG_PATH
    config = {}
    for filepath in Path(config_path).glob('*.yaml'):
        with filepath.open('r') as f:
            config.update(yaml.safe_load(f))

    global CONFIG
    CONFIG = config
    return config


load_config()
