from pathlib import Path
import os

import luigi


def symlinkable(run_method):
    def symlinkable_run(self):
        if self.symlink_from:
            seed_filepath = Path(self.symlink_from) / Path(self.output().path).name
            if seed_filepath.exists():
                yield SymlinkFile(source=seed_filepath, target=self.output().path)
                return

        run_method(self)

    return symlinkable_run


class SymlinkFile(luigi.Task):
    source = luigi.Parameter()
    target = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.target)

    def run(self):
        os.symlink(str(Path(self.source).absolute()),
                   self.target)
