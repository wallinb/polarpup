from pathlib import Path

import luigi
import pandas as pd

import polarpup.core.granules as granules
import polarpup.run as run
from polarpup.luigitasks.base import symlinkable
from polarpup.luigitasks.corrstats import ComputeCorrStats
from polarpup.luigitasks.mask import ComputeMask


class ComputeRoughness(luigi.Task):
    filepath = luigi.Parameter()
    output_dir = luigi.Parameter('./')
    symlink_from = luigi.Parameter(default='')

    def output(self):
        output_filename = granules.roughness_filename(self.filepath)
        return luigi.LocalTarget(str(Path(self.output_dir) / output_filename))

    @symlinkable
    def run(self):
        run.generate_roughness(filepath=self.filepath,
                               output_dir=self.output_dir)


class RoughnessBatch(luigi.WrapperTask):
    filelist = luigi.Parameter()
    output_dir = luigi.Parameter('./')
    symlink_from = luigi.Parameter(default='')

    def requires(self):
        with open(self.filelist, 'rb') as fin:
            granules = pd.read_parquet(fin)

        return [ComputeRoughness(filepath,
                                 output_dir=self.output_dir,
                                 symlink_from=self.symlink_from)
                for filepath in granules['filepaths']]


class ComputeMaskedRoughness(luigi.Task):
    filepath = luigi.Parameter()
    directory = luigi.Parameter(default=None)
    output_dir = luigi.Parameter('./')
    symlink_from = luigi.Parameter(default='')

    def requires(self):
        return {
            'corr_stats': ComputeCorrStats(filepath=self.filepath,
                                           directory=self.directory,
                                           output_dir=self.output_dir,
                                           symlink_from=self.symlink_from),
            'mask': ComputeMask(filepath=self.filepath,
                                directory=self.directory,
                                output_dir=self.output_dir,
                                symlink_from=self.symlink_from),
            'roughness': ComputeRoughness(filepath=self.filepath,
                                          output_dir=self.output_dir,
                                          symlink_from=self.symlink_from)
        }

    def output(self):
        output_filename = granules.masked_filename(self.input()['roughness'].path)
        return luigi.LocalTarget(str(Path(self.output_dir) / output_filename))

    @symlinkable
    def run(self):
        run.generate_corr_stats(filepath=self.filepath,
                                search_dir=self.directory,
                                output_dir=self.output_dir)
        run.generate_mask(filepath=self.filepath,
                          corr_stats_filepath=self.input()['corr_stats'].path,
                          output_dir=self.output_dir)
        run.apply_mask(filepath=self.input()['roughness'].path,
                       mask_filepath=self.input()['mask'].path,
                       output_dir=self.output_dir)
