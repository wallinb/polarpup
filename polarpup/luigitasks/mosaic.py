from pathlib import Path
import os

import luigi

from polarpup.luigitasks.base import symlinkable
import polarpup.run as run


class LastInMosaic(luigi.Task):
    filelist = luigi.Parameter()
    output_dir = luigi.Parameter(default='./')
    symlink_from = luigi.Parameter(default='')

    def input(self):
        with open(self.filelist) as f:
            return [luigi.LocalTarget(filepath.strip()) for filepath in f]

    def output(self):
        basename = os.path.commonprefix([Path(el.path).name for el in self.input()])
        output_filepath = Path(self.output_dir) / (basename + '_mosaic.tif')

        return luigi.LocalTarget(str(output_filepath))

    @symlinkable
    def run(self):
        run.last_in_mosaic(filelist=self.input(),
                           output_filepath=self.output().path)
