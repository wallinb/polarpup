import pickle
from pathlib import Path

import luigi

import polarpup.core.granules as granules
from polarpup.luigitasks.base import symlinkable
import polarpup.run as run


class ComputeCorrStats(luigi.Task):
    filepath = luigi.Parameter()
    directory = luigi.Parameter(default=None)
    output_dir = luigi.Parameter(default='./')
    symlink_from = luigi.Parameter(default=None)

    def output(self):
        filepath = Path(self.filepath)
        output_filename = granules.corr_stats_filename(filepath)

        return luigi.LocalTarget(str(Path(self.output_dir) / output_filename))

    @symlinkable
    def run(self):
        run.generate_corr_stats(filepath=self.filepath,
                                output_dir=self.output_dir,
                                search_dir=self.directory)


class ComputeCorrStatsBatch(luigi.Task):
    filelist = luigi.Parameter()
    input_directory = luigi.Parameter(default=None)
    output_dir = luigi.Parameter(default='./')
    symlink_from = luigi.Parameter(default=None)

    def requires(self):
        with open(self.filelist, 'rb') as fin:
            granules = pickle.load(fin)

        return [ComputeCorrStats(filepath,
                                 output_dir=self.output_dir,
                                 symlink_from=self.symlink_from)
                for filepath in granules['filepaths']]
