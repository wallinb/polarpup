from pathlib import Path

import luigi

from polarpup.luigitasks.base import symlinkable
import polarpup.run as run


class FindGranules(luigi.Task):
    output_dir = luigi.Parameter('./')
    dataproduct = luigi.Parameter(default='landsat8')
    search_dir = luigi.Parameter(default=None)
    symlink_from = luigi.Parameter(default=None)
    daterange = luigi.DateIntervalParameter(None)
    pieces = luigi.Parameter(None)

    def output(self):
        return luigi.LocalTarget(str(Path(self.output_dir) / 'local-granules-index.parquet'))

    @symlinkable
    def run(self):
        start_date = self.daterange.date_a if self.daterange else None
        end_date = self.daterange.date_b if self.daterange else None

        run.find_granules(search_dir=self.search_dir,
                          dataproduct=self.dataproduct,
                          output_filepath=self.output().path,
                          start_date=start_date,
                          end_date=end_date,
                          pieces=eval(self.pieces))


class GetGoogleLandsat8Index(luigi.Task):
    output_dir = luigi.Parameter('./')
    symlink_from = luigi.Parameter(default=None)

    def output(self):
        filepath = Path(self.output_dir) / 'landsat8-google-index.parquet'

        return luigi.LocalTarget(str(filepath))

    @symlinkable
    def run(self):
        run.index_google(output_filepath=self.output().path)


class GetGlobusIndex(luigi.Task):
    directory = luigi.Parameter('landsat_collection1')
    output_dir = luigi.Parameter('./')
    symlink_from = luigi.Parameter(default=None)

    def output(self):
        return luigi.LocalTarget(str(Path(self.output_dir) / 'globus-index.parquet'))

    @symlinkable
    def run(self):
        run.index_globus(directory=self.directory,
                         output_filepath=self.output().path)
