import luigi

import polarpup.run as run


class CancelGlobusTransfers(luigi.Task):
    def run(self):
        run.cancel_globus()
