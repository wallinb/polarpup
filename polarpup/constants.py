from collections import namedtuple
from pathlib import Path

POLARPUP_ROOT = Path(__file__).parent.parent

WRS2_FILEPATH = f'{POLARPUP_ROOT}/assets/wrs2_asc_desc.shp'
GSHHS_ANT_ICE_COARSE_FILEPATH = (
    '/Users/wallinb/data/gshhg-shp-2.3.5-1/GSHHS_shp/{level}/GSHHS_{level}_L5.shp'
)
GOOGLE_SCENELIST_URL = 'https://storage.googleapis.com/gcp-public-data-landsat/index.csv.gz'
AWS_SCENELIST_URL = 'http://landsat-pds.s3.amazonaws.com/c1/L8/scene_list.gz'

GLOBUS_CLIENT_ID = '2795773a-780f-4ed2-a2d5-5feba2f26225'
GOLIVE_ENDPOINT_ID = 'e670ff56-a217-11e7-ad8e-22000a92523b'
LAPTOP_ENDPOINT_ID = '8b2e5282-02c6-11e8-a65a-0a448319c2f8'


_DATAPRODUCTS = ('LANDSAT8', 'NSIDC0710')
DATAPRODUCTS = namedtuple('DATAPRODUCT', _DATAPRODUCTS)(*_DATAPRODUCTS)
