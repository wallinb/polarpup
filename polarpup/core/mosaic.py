import math
import subprocess
from pathlib import Path

import rasterio
from rasterio import Affine
import numpy as np


def gdal_merge(granules, output_filepath):
    input_files = list(granules['filepaths'])
    output_filepath = Path(output_filepath).absolute()
    subprocess.check_call(['gdal_merge.py'] + input_files + ['-o', str(output_filepath)],
                          cwd=output_filepath.parent)
    subprocess.check_call(['gdal_edit.py', '-a_nodata', '0', str(output_filepath)],
                          cwd=output_filepath.parent)


def mosaic(datasets, reducer, res=None):
    if res is None:
        res = min(abs(dataset['x'][1] - dataset['y'][0]) for dataset in datasets)

    boundataset = [{d: (float(dataset[d].min()), float(dataset[d].max()))
                    for d in dataset.dims}
                   for dataset in datasets]

    left = min(b['x'][0] for b in bounds)
    right = max(b['x'][0] for b in bounds)
    bottom = min(b['y'][0] for b in bounds)
    top = max(b['y'][0] for b in bounds)

    output_transform = Affine.translation(left, top)
    output_transform *= Affine.scale(res[0], -res[1])

    output_width = int(math.ceil((right - left) / res[0]))
    output_height = int(math.ceil((top - bottom) / res[1]))

    right, bottom = output_transform*(output_width, output_height)

    dest = np.zeros((output_height, output_width), dtype=dtype)

    for dataset in datasets:
        pass
