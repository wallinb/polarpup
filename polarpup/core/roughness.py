import numpy as np
from scipy import ndimage
import rasterio


def _highpass_esri5x5(data, mask):
    kernel = np.array([[-1, -1, -1, -1, -1],
                       [-1, -1, -1, -1, -1],
                       [-1, -1, 24, -1, -1],
                       [-1, -1, -1, -1, -1],
                       [-1, -1, -1, -1, -1]], dtype=float)
    filtered = ndimage.convolve(data, kernel)
    n, n = kernel.shape
    mask = ndimage.morphology.binary_dilation(mask, iterations=n*2)

    return filtered, mask


def _lowpass_boxcar(data, mask, n=17):
    kernel = np.ones((n, n), dtype=float)/(n*n)
    filtered = ndimage.convolve(data, kernel)
    mask = ndimage.morphology.binary_dilation(mask, iterations=n*2)

    return filtered, mask


def compute_roughness(data, mask):
    # Perform filtering ignoring nodata values in convolution
    data, mask = _highpass_esri5x5(data, mask)
    data = np.absolute(data)
    data, mask = _lowpass_boxcar(data, mask, n=17)

    return data, mask


def compute_roughness_lowmem(input_raster, output_raster, chunks=4, nodata=0):
    hp_n, lp_n = 5, 17
    pad = hp_n + lp_n
    rows, cols = input_raster.shape
    window_height, window_width = int(np.ceil(rows/chunks)), int(np.ceil(cols/chunks))
    for i in range(chunks):
        for j in range(chunks):
            row_start = i*window_height
            col_start = j*window_width
            row_end = min(rows, (i + 1)*window_height)
            col_end = min(cols, (j + 1)*window_width)

            padded_row_start = max(0, i*window_height - pad)
            padded_col_start = max(0, j*window_width - pad)
            padded_row_end = min(rows, (i + 1)*window_height + pad)
            padded_col_end = min(cols, (j + 1)*window_width + pad)

            window = (row_start, row_end), (col_start, col_end)
            padded_window = (padded_row_start, padded_row_end), (padded_col_start, padded_col_end)

            data = input_raster.read(1, window=padded_window).astype('float32')
            mask = (data == nodata)

            result, mask = _highpass_esri5x5(data, mask)
            mask = ndimage.morphology.binary_dilation(mask, iterations=hp_n*2)
            result = np.absolute(result)
            result, mask = _lowpass_boxcar(result, mask, n=17)
            mask = ndimage.morphology.binary_dilation(mask, iterations=lp_n*2)
            result[mask] = nodata

            cropped_result = result[pad:(pad + window_height),
                                    pad:(pad + window_width)]

            output_raster.write_band(1, cropped_result, window=window)


def resample(data, mask, affine, factor=1/20.):
    # Reduce resolution
    data = ndimage.zoom(data, zoom=factor, order=0, mode='nearest')
    mask = ndimage.zoom(mask, zoom=factor, order=0, mode='nearest')

    reduced_affine = rasterio.Affine(
        affine.a / factor, affine.b, affine.c,
        affine.d, affine.e / factor, affine.f
    )

    return data, mask, reduced_affine
