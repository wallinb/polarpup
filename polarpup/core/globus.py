from pathlib import Path
import itertools

import globus_sdk
import pandas as pd

from polarpup.constants import GOLIVE_ENDPOINT_ID, LAPTOP_ENDPOINT_ID
from polarpup.constants import GLOBUS_CLIENT_ID

AUTHORIZER = None
GLOBAUTH_PATH = Path('./.globauth')


def stage(source_filepath, dest_filepath):
    transfer_client = globus_sdk.TransferClient(authorizer=get_authorizer())
    transfer_data = globus_sdk.TransferData(transfer_client,
                                            GOLIVE_ENDPOINT_ID,
                                            LAPTOP_ENDPOINT_ID,
                                            label="Luigi-roughness",
                                            sync_level="checksum",
                                            notify_on_success=False,
                                            notify_on_failure=True,
                                            notify_on_inactive=True)
    transfer_data.add_item(source_filepath, dest_filepath)
    task_response = transfer_client.submit_transfer(transfer_data)

    task_id = task_response['task_id']
    result = transfer_client.task_wait(task_id, timeout=1000)
    if not result:
        raise Exception('Transfer timed out')

    events = transfer_client.task_event_list(task_id)
    if any(event['is_error'] for event in events):
        raise Exception('There was an error with the Globus transfer task')


def walk(authorizer=None, directory=None, transfer_client=None):
    if transfer_client is None:
        transfer_client = globus_sdk.TransferClient(authorizer=authorizer)

    def recurse(directory):
        directory = Path('~/') if directory is None else Path(directory)
        filepaths = []
        listing = list(transfer_client.operation_ls(GOLIVE_ENDPOINT_ID, path=str(directory)))

        filepaths.append(str(directory / el['name']) for el in listing if el['type'] == 'file')

        directories = [directory / el['name'] for el in listing if el['type'] == 'dir']
        for directory in directories:
            filepaths.extend(recurse(directory))

        return filepaths

    return itertools.chain(*recurse(directory))


def get_authorizer():
    global AUTHORIZER
    if AUTHORIZER is None:
        client = globus_sdk.NativeAppAuthClient(GLOBUS_CLIENT_ID)
        if GLOBAUTH_PATH.exists():
            with GLOBAUTH_PATH.open('r') as f:
                auth_data = f.read().split()
                AUTHORIZER = globus_sdk.RefreshTokenAuthorizer(
                    auth_data[1],
                    client,
                    access_token=auth_data[0],
                    expires_at=int(auth_data[2])
                )
        else:
            AUTHORIZER = authenticate()
            with GLOBAUTH_PATH.open('w') as f:
                f.write(f'{AUTHORIZER.access_token}\n')
                f.write(f'{AUTHORIZER.refresh_token}\n')
                f.write(f'{AUTHORIZER.expires_at}\n')

    return AUTHORIZER


def authenticate():
    client = globus_sdk.NativeAppAuthClient(GLOBUS_CLIENT_ID)
    client.oauth2_start_flow(refresh_tokens=True)

    authorize_url = client.oauth2_get_authorize_url()
    print('Please go to this URL and login: {0}'.format(authorize_url))

    auth_code = input('Please enter the code you get after login here: ').strip()
    token_response = client.oauth2_exchange_code_for_tokens(auth_code)

    globus_transfer_data = token_response.by_resource_server['transfer.api.globus.org']
    transfer_rt = globus_transfer_data['refresh_token']
    transfer_at = globus_transfer_data['access_token']
    expires_at_s = globus_transfer_data['expires_at_seconds']

    authorizer = globus_sdk.RefreshTokenAuthorizer(transfer_rt,
                                                   client,
                                                   access_token=transfer_at,
                                                   expires_at=expires_at_s)
    return authorizer


def get_index(directory):
    authorizer = get_authorizer()
    filepaths = walk(authorizer=authorizer, directory=directory)
    granules = pd.DataFrame.from_dict({'filepath': list(filepaths)})

    return granules
