from urllib import request
from urllib.parse import urlparse

from google.cloud import storage
import geopandas as gpd
import pandas as pd
from numpy import logical_and

from polarpup.constants import GOOGLE_SCENELIST_URL
from polarpup.core.geom import get_wrs2_intersection


def get_index():
    response = request.urlopen(GOOGLE_SCENELIST_URL)
    granules = pd.read_csv(response, compression='gzip')

    granules['COLLECTION_NUMBER'] = granules['COLLECTION_NUMBER'].astype(str)

    return granules


def filter_shapefile(granules, shapefile):
    geodf = gpd.GeoDataFrame.from_file(shapefile)
    pathrows = get_wrs2_intersection(geodf)

    def filt(path, row):
        return ((path, row) in pathrows)

    mask = granules[['WRS_PATH', 'WRS_ROW']].apply(lambda x: filt(*x), axis=1)

    return granules[mask]


def filter_daterange(granules, start, end):
    date_acquired = granules['DATE_ACQUIRED'].astype('datetime64[ns]')
    mask = logical_and(date_acquired >= start, date_acquired <= end)

    return granules[mask]


def stage(source_filepath, dest_filepath):
    client = storage.Client()
    bucket = client.bucket('gcp-public-data-landsat')
    url_parts = urlparse(source_filepath)
    blob_name = url_parts.path
    blob = bucket.get_blob(blob_name)
    blob.download_to_filename(dest_filepath)
