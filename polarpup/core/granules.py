from pathlib import Path

import pandas as pd
import geopandas as gpd
from numpy import logical_and

from polarpup.core.geom import get_wrs2_intersection
import polarpup.dataproducts as dataproducts


def corr_stats_filename(filepath):
    output_filename = Path(filepath).with_suffix('.stats.nc').name

    return output_filename


def roughness_filename(filepath):
    output_filename = Path(filepath).with_suffix('.roughness.nc').name

    return output_filename


def mask_filename(filepath):
    output_filename = Path(filepath).with_suffix('.mask.nc').name

    return output_filename


def masked_filename(filepath):
    filepath = Path(filepath)
    output_filename = filepath.stem + '.masked.nc'

    return output_filename


def filepaths_to_dataframe(filepaths):
    granules = pd.DataFrame.from_dict({
        'filepath': list(filepaths)
    })

    return granules


def augment_with_metadata(granules, dataproduct):
    def is_granule(row):
        return (dataproducts.filename_pattern(dataproduct).match(row.filepath.name) is not None)

    def get_row_metadata(row):
        metadata = dataproducts.get_metadata(dataproduct=dataproduct, filepath=row.filepath)
        return pd.Series(data=metadata, index=metadata.keys())

    granules = granules[granules.apply(is_granule, axis=1)]
    with_metadata = granules.apply(get_row_metadata, axis=1)

    return granules.join(with_metadata)


def find_granules(dataproduct, search_dir, start_date, end_date, pieces):
    filepaths = dataproducts.find_granules(
        dataproduct=dataproduct,
        search_dirs=[search_dir],
        start_date=start_date,
        end_date=end_date,
        pieces=pieces
    )
    granules = filepaths_to_dataframe(filepaths)
    granules = augment_with_metadata(granules=granules, dataproduct=dataproduct)

    # Cleanup
    granules.path = granules.path.astype(int)
    granules.row = granules.row.astype(int)

    return granules


def filter_intersecting_shapefile(granules, shapefile):
    geodf = gpd.GeoDataFrame.from_file(shapefile).to_crs({'init': 'epsg:4326'})
    pathrows = get_wrs2_intersection(geodf)

    filtered = pd.concat([granules[logical_and(granules['path'] == path,
                                               granules['row'] == row)]
                          for path, row in pathrows])

    return filtered


def filter_collected_in_daterange(granules, start, end):
    filtered = pd.concat([granules[granules['date'] == date]
                          for date in pd.date_range(start, end)])

    return filtered
