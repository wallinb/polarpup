from pathlib import Path
from datetime import timedelta

import xarray
import pandas as pd
import numpy as np

from polarpup.dataproducts.api import find_granules, get_metadata
from polarpup.core.granules import augment_with_metadata
from polarpup.constants import DATAPRODUCTS


def compute_corr_stats(filepath, search_dir=None, granule_index=None, max_dt=16*3):
    filepath = Path(filepath)
    metadata = get_metadata(dataproduct=DATAPRODUCTS.LANDSAT8, filepath=filepath)
    date = metadata['collected_date']
    path = metadata['path']
    row = metadata['row']
    start_date = date - timedelta(days=max_dt)
    end_date = date + timedelta(days=max_dt + 1)

    if granule_index is None:
        # Find NSIDC0710 granules derived from the given landsat filepath
        search_dir = str(filepath.parent) if search_dir is None else search_dir
        filepaths = set(find_granules(
            dataproduct=DATAPRODUCTS.NSIDC0710,
            start_date=start_date,
            end_date=end_date,
            pieces={'path': path, 'row': row},
            search_dirs=[search_dir]))

        if not filepaths:
            raise RuntimeError(f'No NSIDC0710 granules found matching {filepath} in {search_dir}')

        # Load into dataframe
        granules = pd.DataFrame.from_dict({'filepath': list(filepaths)})
        # Add metadata columns
        granules = augment_with_metadata(granules=granules, dataproduct=DATAPRODUCTS.NSIDC0710)
    else:
        granules = granule_index[np.logical_and(granule_index.path == int(path),
                                                granule_index.row == int(row))]

    # Filter to daterange
    def date_filter(row):
        return all(start_date <= d.date() < end_date
                   for d in (row['date_a'], row['date_b']))
    granules = granules[granules.apply(date_filter, axis=1)]

    # Primary granules contain target date
    def primary_filter(row):
        return any(d.date() == date
                   for d in (row['date_a'], row['date_b']))
    primary_mask = granules.apply(primary_filter, axis=1)

    # Secondary granules contain dates paired with target date
    primary_dateset = {el.date()
                       for el in set(granules[primary_mask].date_a).union(
        set(granules[primary_mask].date_b)
    )}
    secondary_dateset = primary_dateset.difference(set([date]))

    def secondary_filter(row):
        return all(d.date() in secondary_dateset
                   for d in (row['date_a'], row['date_b']))
    secondary_mask = granules.apply(secondary_filter, axis=1)

    primary_granules = granules[primary_mask]
    secondary_granules = granules[secondary_mask]

    primary = xarray.open_mfdataset(primary_granules.filepath,
                                    concat_dim='granules')
    secondary = xarray.open_mfdataset(secondary_granules.filepath,
                                      concat_dim='granules')

    all_granules = xarray.concat([primary, secondary], dim='granules')

    variables = {
        'corr_mean': all_granules.corr.mean(dim='granules'),
        'corr_var': all_granules.corr.var(dim='granules'),
        'corr_max': primary.corr.max(dim='granules'),
        'corr_mean_primary': primary.corr.mean(dim='granules'),
        'corr_mean_secondary': secondary.corr.mean(dim='granules'),
        'corr_ratio': primary.corr.mean(dim='granules')/secondary.corr.mean(dim='granules'),
        'corr_max_ratio': primary.corr.max(dim='granules')/secondary.corr.max(dim='granules')
    }

    for name, variable in variables.items():
        variable.name = name
        variable.attrs['grid_mapping'] = 'polar_stereographic'

    stats = xarray.merge(variables.values())
    stats['polar_stereographic'] = primary.polar_stereographic[0]
    stats.attrs['primary_cnt'] = len(primary_granules)
    stats.attrs['primary_set'] = ' '.join([str(el) for el in primary_granules.filepath])
    stats.attrs['secondary_cnt'] = len(secondary_granules.filepath)
    stats.attrs['secondary_set'] = ' '.join([str(el) for el in secondary_granules.filepath])

    return stats
