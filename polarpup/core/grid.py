import xarray
import numpy as np
import rasterio
from rasterio import Affine
from rasterio.crs import CRS
from scipy import ndimage


GRIDMAPPING = 'polar_stereographic'
GRIDMAPPING_WKT = (
    'PROJCS["WGS 84 / Antarctic Polar Stereographic",'
    '    GEOGCS["WGS 84",'
    '        DATUM["WGS_1984",'
    '            SPHEROID["WGS 84",6378137,298.257223563,'
    '                AUTHORITY["EPSG","7030"]],'
    '            AUTHORITY["EPSG","6326"]],'
    '        PRIMEM["Greenwich",0,'
    '            AUTHORITY["EPSG","8901"]],'
    '        UNIT["degree",0.0174532925199433,'
    '            AUTHORITY["EPSG","9122"]],'
    '        AUTHORITY["EPSG","4326"]],'
    '    PROJECTION["Polar_Stereographic"],'
    '    PARAMETER["latitude_of_origin",-71],'
    '    PARAMETER["central_meridian",0],'
    '    PARAMETER["scale_factor",1],'
    '    PARAMETER["false_easting",0],'
    '    PARAMETER["false_northing",0],'
    '    UNIT["metre",1,AUTHORITY["EPSG","9001"]],'
    '    AXIS["Easting",EAST],'
    '    AXIS["Northing",NORTH],'
    '    AUTHORITY["EPSG","3031"]]'
)
POLARSTEREO_CRS = CRS({
    'init': 'epsg:3031'
})
GRIDMAPPING_ATTRS = {
    'GeoTransform': '-523207.5 300.0 0 -989992.5 0 -300.0',
    'false_easting': 0.0,
    'false_northing': 0.0,
    'grid_mapping_name': 'polar_stereographic',
    'inverse_flattening': 298.25722356300003,
    'latitude_of_projection_origin': -90.0,
    'longitude_of_prime_meridian': 0.0,
    'semi_major_axis': 6378137.0,
    'standard_parallel': -71.0,
    'straight_vertical_longitude_from_pole': 0.0,
    'spatial_ref': GRIDMAPPING_WKT
}
X_DIM, Y_DIM = 'x', 'y'


def init_polarstereo_dataset(shape, affine):
    rows, cols = shape
    x, _ = affine * (np.arange(cols), np.zeros((cols,)))
    _, y = affine * (np.zeros((rows,)), np.arange(rows))
    ds = xarray.Dataset({GRIDMAPPING: None},
                        coords={Y_DIM: y, X_DIM: x})
    ds[GRIDMAPPING].attrs.update(GRIDMAPPING_ATTRS)
    ds[X_DIM].attrs['standard_name'] = 'projection_x_coordinate'
    ds[X_DIM].attrs['long_name'] = 'x coordinate of projection'
    ds[X_DIM].attrs['units'] = 'm'
    ds[Y_DIM].attrs['standard_name'] = 'projection_y_coordinate'
    ds[Y_DIM].attrs['long_name'] = 'y coordinate of projection'
    ds[Y_DIM].attrs['units'] = 'm'

    return ds


def add_polarstereo_grid(dataset, name, data):
    dataset[name] = ((Y_DIM, X_DIM), data)
    dataset[name].attrs['grid_mapping'] = GRIDMAPPING

    return dataset[name]


def get_affine(grid):
    delta_x = float(grid.x[1] - grid.x[0])
    delta_y = float(grid.y[1] - grid.y[0])
    x0 = float(grid.x[0])
    y0 = float(grid.y[0])

    affine = Affine(delta_x, 0, x0,
                    0, delta_y, y0)

    return affine


def get_shape(grid):
    shape = (len(grid.y), len(grid.x))

    return shape


def resample(data, affine, factor=1/20.):
    # Reduce resolution
    data = ndimage.zoom(data, zoom=factor, order=0, mode='nearest')

    reduced_affine = Affine(
        affine.a / factor, affine.b, affine.c,
        affine.d, affine.e / factor, affine.f
    )

    return data, reduced_affine


def load_landsat(filepath):
    with rasterio.open(filepath) as raster:
        affine = raster.affine
        assert raster.crs == POLARSTEREO_CRS
        data = raster.read(1).astype(float)

    dataset = init_polarstereo_dataset(shape=data.shape, affine=affine)
    add_polarstereo_grid(dataset=dataset, name='values', data=data)

    return dataset
