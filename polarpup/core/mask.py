import xarray


def compute_mask(filepath, corr_stats_filepath, output_dir):
    corr_stats_ds = xarray.open_dataset(corr_stats_filepath)

    masks = {
        'corr_max_thresh': (corr_stats_ds.corr_max < .3),
        'corr_ratio': (corr_stats_ds.corr_ratio < 1),
        'corr_max_ratio': (corr_stats_ds.corr_max_ratio < 1)
    }

    return masks
