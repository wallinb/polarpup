import geopandas as gpd

from polarpup.constants import WRS2_FILEPATH, GSHHS_ANT_ICE_COARSE_FILEPATH


def load_wrs2():
    return gpd.GeoDataFrame.from_file(WRS2_FILEPATH).to_crs({'init': 'epsg:4326'})


def load_antarctica_ice_boundary(level='c'):
    return gpd.GeoDataFrame.from_file(
        GSHHS_ANT_ICE_COARSE_FILEPATH.format(level=level)
    ).to_crs({'init': 'epsg:4326'})


def get_wrs2_intersection(geodf):
    wrs2 = load_wrs2()
    pathrows = set()
    for _, tile in wrs2.iterrows():
        for _, shape in geodf.iterrows():
            if tile['geometry'].intersects(shape['geometry']):
                pathrows.add((tile['PATH'], tile['ROW']))

    return pathrows
