import logging
import datetime
import os

from colorlog import ColoredFormatter

logger = logging.getLogger()


FILE_FORMATTER = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

CONSOLE_FORMATTER = ColoredFormatter(
    "%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s",
    datefmt=None,
    reset=True,
    log_colors={
            'DEBUG':    'cyan',
            'INFO':     'green',
            'WARNING':  'yellow',
            'ERROR':    'red',
            'CRITICAL': 'red,bg_white',
        },
    secondary_log_colors={},
    style='%'
)


def init_logging(log_level, dir=None):
    logger.setLevel(log_level)
    add_console_logging(logger, log_level)
    if dir is not None:
        add_file_logging(logger, dir, log_level)


def add_console_logging(logger, level):
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = CONSOLE_FORMATTER
    ch.setFormatter(formatter)
    logger.addHandler(ch)


def add_file_logging(logger, dir, level):

    if not os.path.exists(dir):
        os.makedirs(dir)

    now = datetime.datetime.now().strftime('%Y-%m-%d.%H%M%S')
    log_filename = '{}.{}.log'.format('pmpipe', now)
    file_handler = logging.FileHandler(os.path.join(dir, log_filename))

    file_handler.setFormatter(FILE_FORMATTER)
    file_handler.setLevel(level)
    logger.addHandler(file_handler)
