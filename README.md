Polarpup
========

Polarpup goes "rough rough!"

This repository contains code for generating roughness measures from landsat imagery, as well as cloud masks based on NSIDC0710 outputs.
