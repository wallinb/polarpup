FROM continuumio/miniconda3
RUN apt-get update; apt-get upgrade -y; apt-get dist-upgrade -y; apt-get autoremove -y; apt-get autoclean -y; sync

# Install environment
COPY ./ /polarpup
WORKDIR /polarpup
RUN conda update -n base conda; sync
RUN conda env update -f environment.yml -n root; sync

# Install source
RUN python setup.py develop; sync
