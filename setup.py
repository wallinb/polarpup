from setuptools import setup, find_packages

setup(name='polarpup',
      version='0.0.1',
      description='Algorithms for polar roughness',
      url='git@bitbucket.org:wallinb/polarpup.git',
      author='Bruce Wallin',
      author_email='bruce.wallin@nsidc.org',
      packages=find_packages(exclude=('tasks',)),
      entry_points={
          'console_scripts': [
              'polarpup = polarpup.cli:polarpup',
              ]
          },
      include_package_data=True)
